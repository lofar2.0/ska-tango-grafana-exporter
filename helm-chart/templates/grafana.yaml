apiVersion: v1
kind: Service
metadata:
  name: grafana-service
  namespace: {{ .Release.Namespace }}
spec:
  selector:
    app: grafana-service
  type: NodePort
  ports:
  - protocol: TCP
    port: 3000
    targetPort: 3000

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: grafana-provisioning-datasource
  namespace: {{ .Release.Namespace }}
data: 
  datasource.yaml: |- 
    apiVersion: 1
    deleteDatasources:
      - name: 'Prometheus'
        orgId: 1
      - name: 'TangoDB'
        orgId: 1
      - name: 'TangoArchiver'
        orgId: 1
      - name: 'ElasticEngageCluster'
        orgId: 1
      - name: 'ElasticLocal'
        orgId: 1
    datasources:
    - name: 'Prometheus'
      type: 'prometheus'
      access: 'proxy'
      orgId: 1
      url: http://prometheus-service:9090
      basicAuth: false
      isDefault: true
      editable: false
    - name: 'TangoDB'
      type: 'mysql'
      access: 'proxy'
      orgId: 1
      url: {{ .Values.grafana.tangodb }}
      user: 'tango'
      database: 'tango'
      secureJsonData:
        password: 'tango'
    - name: 'TangoArchiver'
      type: 'mysql'
      access: 'proxy'
      orgId: 1
      url: {{ .Values.grafana.archiverdb }}
      user: 'tango'
      database: 'hdbpp'
      secureJsonData:
        password: 'tango'
    - name: 'ElasticEngageCluster'
      type: 'elasticsearch'
      access: 'proxy'
      orgId: 1
      url: http://{{ .Values.grafana.elastic_remote }}
      database: 'filebeat-*'
      jsonData: 
        esVersion: 70
        logLevelField: ''
        logMessageField: 'ska_log_message'
        maxConcurrentShardRequests: 5
        timeField: '@timestamp'
        timeInterval: '10s'
{{ if .Values.grafana.elastic_local.enabled }}
    - name: 'ElasticLocal'
      type: 'elasticsearch'
      access: 'proxy'
      orgId: 1
      url: http://{{ .Values.grafana.elastic_local.host }}
      database: 'logstash-*'
      jsonData: 
        esVersion: 70
        logLevelField: ''
        logMessageField: 'ska_log_message'
        maxConcurrentShardRequests: 5
        timeField: '@timestamp'
        timeInterval: '10s'
{{ end }}

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: grafana-provisioning-dashboards
  namespace: {{ .Release.Namespace }}
data: 
  archiver.json: |-
{{ ($.Files.Get "data/dashboards/archiver.json") | indent 4  }}
  dashboard.json: |-
{{ ($.Files.Get "data/dashboards/dashboard.json") | indent 4  }}
  db.json: |-
{{ ($.Files.Get "data/dashboards/db.json") | indent 4  }}
  state.json: |-
{{ ($.Files.Get "data/dashboards/state.json") | indent 4  }}
  mccs.json: |-
{{ ($.Files.Get "data/dashboards/mccs.json") | indent 4  }}


---
apiVersion: v1
kind: ConfigMap
metadata:
  name: grafana-provisioning-example-definition
  namespace: {{ .Release.Namespace }}
data: 
  example.yml: |
    apiVersion: 1

    providers:
    - name: 'Example Dashboards'
      orgId: 1
      folder: 'examples'
      type: file
      disableDeletion: false
      updateIntervalSeconds: 3
      editable: true
      options:
        path: '/etc/grafana/provisioning/dashboards/examples'
  grafana.ini: |
    [security]
    allow_embedding = true

---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: grafana-deployment
  namespace: {{ .Release.Namespace }}
  labels:
    app: grafana-service
spec:
  serviceName: grafana-service
  replicas: 1
  selector:
    matchLabels:
      app: grafana-service
  template:
    metadata:
      labels:
        app: grafana-service
    spec:
      containers:
      - name: grafana
        image: "{{ .Values.grafana.image.registry }}/{{ .Values.grafana.image.image }}:{{ .Values.grafana.image.tag }}"
        imagePullPolicy: {{ .Values.grafana.image.pullPolicy }}
        ports:
        - containerPort: 3000
        env:
        - name: GF_PANELS_DISABLE_SANITIZE_HTML
          value: "true"
        volumeMounts:
        - name: data
          mountPath: /var/lib/grafana
        - name: config
          mountPath: /etc/grafana/provisioning/datasources
        - name: exampledashboards
          mountPath: /etc/grafana/provisioning/dashboards/examples
        - name: exampledef
          mountPath: /etc/grafana/provisioning/dashboards/example.yml
          subPath: example.yml
        - name: exampledef
          mountPath: /etc/grafana/grafana.ini
          subPath: grafana.ini
      volumes:
      - name: config
        configMap:
          name: grafana-provisioning-datasource
      - name: exampledashboards
        configMap:
          name: grafana-provisioning-dashboards
      - name: customini
        configMap:
          name: grafana-custom-ini-definition
      - name: exampledef
        configMap:
          name: grafana-provisioning-example-definition
      - name: data
        persistentVolumeClaim:
          claimName: grafana-tango-grafana

