import { PanelOptionsEditorBuilder } from '@grafana/data';
import { WebjiveOptions } from './types';

export const optionsBuilder = (builder: PanelOptionsEditorBuilder<WebjiveOptions>) => {
  // Global options
  builder
    .addTextInput({
      path: 'auth_url',
      name: 'Authentication url',
      description: 'Webjive authentication url',
      defaultValue: 'http://tangogql-proxy.integration.engageska-portugal.pt/login_webjive',
    })
    .addTextInput({
      path: 'dashboard_url',
      name: 'Dashboards url',
      description: 'Webjive dashboard_url url',
      defaultValue: 'http://mid.integration.engageska-portugal.pt/testdb',
    });
};
