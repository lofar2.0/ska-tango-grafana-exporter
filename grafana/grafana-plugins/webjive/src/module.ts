import { PanelPlugin } from '@grafana/data';
import { WebjiveOptions } from './types';
import { WebjivePanel } from './WebjivePanel';
import { optionsBuilder } from './options';

export const plugin = new PanelPlugin<WebjiveOptions>(WebjivePanel).setNoPadding().setPanelOptions(optionsBuilder);
