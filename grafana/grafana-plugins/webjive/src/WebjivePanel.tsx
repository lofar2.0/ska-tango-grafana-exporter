import React from 'react';
import { PanelProps } from '@grafana/data';
import { WebjiveOptions } from 'types';

interface Props extends PanelProps<WebjiveOptions> {}

export const WebjivePanel: React.FC<Props> = ({ options, data, width, height }) => {
  const result = fetch(options.auth_url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json',
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
  });
  console.log(result);

  return <iframe id="frame" name="frame" height="100%" width="100%" src={options.dashboard_url}></iframe>;
};
