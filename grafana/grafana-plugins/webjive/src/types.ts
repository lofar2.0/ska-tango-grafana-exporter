export interface WebjiveOptions {
  auth_url: string;
  dashboard_url: string;
}
