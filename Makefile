
CHART_VERSION=1.0.1
DIRS = exporter grafana tangogql-proxy
BUILDDIRS = $(DIRS:%=build-%)
PUSHDIRS = $(DIRS:%=push-%)
HELM_HOST ?= https://nexus.engageska-portugal.pt
MINIKUBE ?= true

TARGET_NAMESPACE ?=integration-mid##Target namespace
DATABASEDS ?=databaseds-tango-base-test##tango databaseds pod name
TANGO_HOST ?=$(DATABASEDS).$(TARGET_NAMESPACE).svc.cluster.local:10000
TANGODB ?=tango-base-tangodb.$(TARGET_NAMESPACE).svc.cluster.local:3306## TANGO DB data source
ARCHIVERDB ?=archiver-hdbppdb-db.$(TARGET_NAMESPACE).svc.cluster.local:3306## ARCHIVER DB data source
ELASTIC ?=192.168.93.94:9200## Elastic data source
WEBJIVE ?=webjive-webjive-test-mid## Webjive pod name
WEBJIVE_AUTH_URL ?=http://$(WEBJIVE).$(TARGET_NAMESPACE).svc.cluster.local:8080/login##Webjive authentication url
TANGOGQL_URL ?=http://$(WEBJIVE).$(TARGET_NAMESPACE).svc.cluster.local:5004/db
WEBJIVE_USER ?="mandatory"## webjive username
WEBJIVE_PASS ?="mandatory"## webjive password

GRAFANA_INGRESS_HOST ?=grafana.integration.engageska-portugal.pt##for grafana
TANGOGQL_PROXY_INGRESS_HOST ?=tangogql-proxy.integration.engageska-portugal.pt##for grafana

CUSTOM_VALUES ?= --set tango_gql_proxy.webjive_auth_url=$(WEBJIVE_AUTH_URL) \
				 --set tango_gql_proxy.webjive_user=$(WEBJIVE_USER) \
				 --set tango_gql_proxy.webjive_pass=$(WEBJIVE_PASS) \
				 --set tango_gql_proxy.tangogql_url=$(TANGOGQL_URL) \
				 --set grafana.tangodb=$(TANGODB) \
				 --set grafana.archiverdb=$(ARCHIVERDB) \
				 --set tango_exporter.tango_host=$(TANGO_HOST) \
				 --set grafana.ingress_host=$(GRAFANA_INGRESS_HOST) \
				 --set grafana.elastic_remote=$(ELASTIC) \
				 --set tango_gql_proxy.ingress_host=$(TANGOGQL_PROXY_INGRESS_HOST)

-include PrivateRules.mak

.DEFAULT_GOAL := help

namespace: ## create the kubernetes namespace
	@kubectl describe namespace tango-grafana > /dev/null 2>&1 ; \
		K_DESC=$$? ; \
		if [ $$K_DESC -eq 0 ] ; \
		then kubectl describe namespace tango-grafana ; \
		else kubectl create namespace tango-grafana; \
		fi

build: $(DIRS)  ## build all images
$(DIRS): $(BUILDDIRS)
$(BUILDDIRS):
	$(MAKE) -C $(@:build-%=%) build

push: $(PUSHDIRS)  ## push images to Docker hub
$(PUSHDIRS):
	$(MAKE) -C $(@:push-%=%) push

help:  ## show this help.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

publish-chart: ## publish chart on the 
	@helm package helm-chart && \
	curl -v -u $(HELM_USERNAME):$(HELM_PASSWORD) --upload-file tango-grafana-$(CHART_VERSION).tgz $(HELM_HOST)/repository/helm-chart/tango-grafana-$(CHART_VERSION).tgz

install-chart: namespace## install the tango-grafana helm chart on the namespace tango-grafana
	helm install tango-grafana0 helm-chart/ --set minikube=$(MINIKUBE) $(CUSTOM_VALUES) --namespace tango-grafana
	kubectl -n tango-grafana wait --for=condition=ready --all --timeout=300s pods

template-chart: ## template the tango-grafana helm chart on the namespace tango-grafana
	helm install tango-grafana0 helm-chart/ --set minikube=$(MINIKUBE) $(CUSTOM_VALUES) --namespace tango-grafana 

uninstall-chart: ## uninstall the tango-grafana helm chart on the namespace tango-grafana
	helm uninstall tango-grafana0 --namespace tango-grafana
	kubectl -n tango-grafana wait --all --for=delete pods --timeout=300s

reinstall-chart: uninstall-chart install-chart ## reinstall the tango-grafana helm chart on the namespace tango-grafana

upgrade-chart: ## upgrade the tango-grafana helm chart on the namespace tango-grafana
	helm upgrade tango-grafana0 --set minikube=$(MINIKUBE) $(CUSTOM_VALUES) helm-chart/ --namespace tango-grafana

.PHONY: subdirs $(DIRS)
.PHONY: subdirs $(BUILDDIRS)
.PHONY: subdirs $(PUSHDIRS)
.PHONY: build push help